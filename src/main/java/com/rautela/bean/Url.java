package com.rautela.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Url {

    private @Id @GeneratedValue Long id;
    private String longUrl;
    private String shortUrl;
    boolean isActive;

    public Url(String longUrl, String shortUrl, boolean isActive) {
	super();
	this.longUrl = longUrl;
	this.shortUrl = shortUrl;
	this.isActive = isActive;
    }

    public Url() {
	super();
    }

}
