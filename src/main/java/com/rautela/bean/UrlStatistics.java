package com.rautela.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class UrlStatistics {

    private @Id @GeneratedValue Long id;

    @ManyToOne
    @JoinColumn
    private Url url;
    private String sourceIp;
    private String sourceDevice;
    private Date date;

    public UrlStatistics(Url url, String sourceIp, String sourceDevice, Date date) {
	super();
	this.url = url;
	this.sourceIp = sourceIp;
	this.sourceDevice = sourceDevice;
	this.date = date;
    }

    public UrlStatistics() {
	super();
    }

}
