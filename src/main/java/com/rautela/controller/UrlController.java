package com.rautela.controller;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rautela.bean.Url;
import com.rautela.service.UrlShortenerService;
import com.rautela.service.UrlStatisticsService;

@CrossOrigin
@RestController
@RequestMapping("/r")
public class UrlController {

    Logger logger = LoggerFactory.getLogger(UrlController.class);
    @Autowired
    private UrlShortenerService urlShortenerService;
    @Autowired
    private UrlStatisticsService urlStatisticsService;
    @Autowired
    private HttpServletRequest request;

    @GetMapping("/{shortUrl}")
    ResponseEntity redirectUrl(@PathVariable String shortUrl) {
	logger.info("Url Expansion request for shortUrl->" + shortUrl);
	Url url = urlShortenerService.findByShortUrl(shortUrl);

	HttpHeaders headers = new HttpHeaders();
	headers.set("Cache-Control", "no-cache, no-store, must-revalidate");
	headers.set("Pragma", "no-cache");
	headers.set("Expires", "0");
	if (url != null && !url.getLongUrl().isEmpty()) {
	    String fullUrl = url.getLongUrl();
	    if (!fullUrl.startsWith("http")) {
		fullUrl = "http://" + fullUrl;
	    }
	    headers.setLocation(URI.create(fullUrl));

	    // TODO - Persist analytics data he
	    urlStatisticsService.save(url, request.getHeader("X-FORWARDED-FOR"), request.getHeader("User-Agent"));
	    logger.info("Url Expansion request for shortUrl >>" + shortUrl + "<< forwarded to >>" + fullUrl + "<<");

	    headers.set("Connection", "close");
	    return new ResponseEntity<>(headers, HttpStatus.MOVED_PERMANENTLY);
	} else {
	    logger.info("Url Expansion request for shortUrl >>" + shortUrl + "<< forwarded to >>error.html<<");

	    headers.setLocation(URI.create("error.html"));
	    return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);

	}

    }

}
