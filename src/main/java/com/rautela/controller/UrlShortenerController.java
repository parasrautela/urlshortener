package com.rautela.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rautela.bean.Url;
import com.rautela.exception.UrlNotFoundException;
import com.rautela.service.UrlShortenerService;

@CrossOrigin
@RestController
@RequestMapping("/urlshortener")
public class UrlShortenerController {

    Logger logger = LoggerFactory.getLogger(UrlShortenerController.class);

    @Autowired
    private UrlShortenerService urlShortenerService;

    @GetMapping("/url")
    ResponseEntity<List<Url>> getUrl() {
	List<Url> urls = urlShortenerService.findAll();
	return new ResponseEntity<>(urls, HttpStatus.OK);

    }

    @GetMapping("/url/{id}")
    ResponseEntity<Url> getUrl(@PathVariable Long id) {
	try {
	    Url url = urlShortenerService.findById(id);
	    if (url != null) {
		return new ResponseEntity<>(url, HttpStatus.OK);
	    } else {
		return ResponseEntity.notFound().build();
	    }
	} catch (UrlNotFoundException e) {
	    return ResponseEntity.notFound().build();
	}
    }

    @PostMapping("/url")
    Url shortenUrl(@RequestParam(value = "url") String url, @RequestParam(value = "customUrl",defaultValue = "") String customUrl) {
	logger.info("Url Short request for Url >>" + url + "<< and customUrl Request >>"+customUrl+"<<");
	if(!url.startsWith("http")) {
	    url = "http://"+url ;
	}
	Url urlObj = customUrl.isEmpty()?urlShortenerService.save(url.toLowerCase()):urlShortenerService.save(url.toLowerCase(),customUrl);
	logger.info("Url Short request for Url >>" + url + "<< Complete. Short Url created >>" + urlObj.getShortUrl()
		+ "<<");
	return urlObj;
    }

    @DeleteMapping("/url/{id}")
    void deleteUrl(@PathVariable Long id) {
	urlShortenerService.deleteById(id);
    }

}
