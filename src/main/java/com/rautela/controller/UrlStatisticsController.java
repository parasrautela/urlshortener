package com.rautela.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rautela.bean.Url;
import com.rautela.bean.UrlStatistics;
import com.rautela.exception.UrlNotFoundException;
import com.rautela.service.UrlStatisticsService;

@CrossOrigin
@RestController
@RequestMapping("/urlstatistics")
public class UrlStatisticsController {

    Logger logger = LoggerFactory.getLogger(UrlStatisticsController.class);

    @Autowired
    private UrlStatisticsService urlStatisticsService;

    @GetMapping("/")
    ResponseEntity<List<UrlStatistics>> getUrlStatistics() {
	List<UrlStatistics> urlstats = urlStatisticsService.findAll();
	return new ResponseEntity<>(urlstats, HttpStatus.OK);

    }

    @GetMapping("/{shortUrl}")
    ResponseEntity<List<UrlStatistics>> getUrlStatistics(@PathVariable String shortUrl) {
	try {
	    List<UrlStatistics> urlstats = urlStatisticsService.findByShortUrl(shortUrl);
	    if (urlstats != null) {
		return new ResponseEntity<>(urlstats, HttpStatus.OK);
	    } else {
		return ResponseEntity.notFound().build();
	    }
	} catch (UrlNotFoundException e) {
	    return ResponseEntity.notFound().build();
	}
    }

   
}
