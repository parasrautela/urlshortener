package com.rautela.exception;

public class UrlCreationFailedException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public UrlCreationFailedException(String message) {
	super(message +" :Unable to shorten this Url!");
    }


}
