package com.rautela.exception;

public class UrlDuplicateException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public UrlDuplicateException(String message) {
	super(message +" is already taken!");
    }


}
