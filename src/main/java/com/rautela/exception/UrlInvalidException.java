package com.rautela.exception;

public class UrlInvalidException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public UrlInvalidException(String message) {
	super(message +" is an invalid Url!");
    }


}
