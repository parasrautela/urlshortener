package com.rautela.exception;

public class UrlNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UrlNotFoundException(Long arg0) {
	super("No Record Exists for id-" + arg0);

    }
    public UrlNotFoundException(String arg0) {
	super("No Record Exists for shortUrl-" + arg0);
    }

}
