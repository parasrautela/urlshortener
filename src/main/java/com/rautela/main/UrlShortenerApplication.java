package com.rautela.main;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.rautela.bean.Url;
import com.rautela.repository.UrlRepository;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = { "com.rautela" })
@EntityScan(basePackages = { "com.rautela.bean" })
@EnableJpaRepositories(basePackages = { "com.rautela.repository" })
public class UrlShortenerApplication {

    public static void main(String[] args) {
	SpringApplication.run(UrlShortenerApplication.class, args);
    }

    @Bean
    public Docket api() {
	return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
		.paths(PathSelectors.any()).build().pathMapping("/").
		tags(new Tag("Url Shortener service", "All apis related to Url Shortener service"));
    }

    @Bean
    public CommandLineRunner loadData(UrlRepository repository) {
	return (args) -> {
	    repository.save(new Url("www.google.com", "QwErTy", true));
	    repository.save(new Url("www.facebook.com", "awErTy", true));
	    repository.save(new Url("www.twitter.com", "QTErTy", true));
	    repository.save(new Url("www.gitlab.com", "ERErTy", true));
	};
    }
}
