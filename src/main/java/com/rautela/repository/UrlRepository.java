package com.rautela.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.rautela.bean.Url;

public interface UrlRepository extends CrudRepository<Url, Long> {

    List<Url> findByLongUrl(String longUrl);

    List<Url> findByShortUrl(String shortUrl);
}