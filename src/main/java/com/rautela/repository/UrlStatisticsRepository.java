package com.rautela.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.rautela.bean.Url;
import com.rautela.bean.UrlStatistics;

public interface UrlStatisticsRepository extends CrudRepository<UrlStatistics, Long>{

    List<UrlStatistics> findByUrl(Url url);
}
