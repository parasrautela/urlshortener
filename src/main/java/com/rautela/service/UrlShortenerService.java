package com.rautela.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rautela.bean.Url;
import com.rautela.exception.UrlCreationFailedException;
import com.rautela.exception.UrlDuplicateException;
import com.rautela.exception.UrlInvalidException;
import com.rautela.exception.UrlNotFoundException;
import com.rautela.repository.UrlRepository;
import com.rautela.utility.Utility;

@Service
public class UrlShortenerService {

    Logger logger = LoggerFactory.getLogger(UrlShortenerService.class);

    @Autowired
    private UrlRepository urlRepository;

    public List<Url> findAll() {
	ArrayList<Url> url = (ArrayList<Url>) urlRepository.findAll();
	return url;
    }

    public Url findById(Long id) throws UrlNotFoundException {
	Url url = urlRepository.findById(id).orElseThrow(() -> new UrlNotFoundException(id));
	return url;
    }

    public void deleteById(Long id) {
	urlRepository.deleteById(id);

    }

    public Url save(String urlString) throws UrlInvalidException {
	// 1. Validity Check
	boolean isValidUrl = Utility.isValidUrl(urlString);
	if (!isValidUrl) {
	    throw new UrlInvalidException(urlString);
	}
	// 2. Duplicate Check
	List<Url> urls = urlRepository.findByLongUrl(urlString);
	if (urls.size() > 0) {
	    return urls.get(0);
	}

	// 3. Create url
	Url url = null;
	int retryCount = 0;
	while (retryCount < 3) {
	    String shortUrl = Utility.getShortUrl(urlString, retryCount);
	    // 3.b Check if short url already present
	    urls = urlRepository.findByShortUrl(shortUrl);
	    if (urls.isEmpty() && !shortUrl.isEmpty()) {
		url = new Url(urlString, shortUrl,true);
		break;
	    } else {
		retryCount++;
	    }

	}
	// 4. Save url
	if (url != null) {
	    urlRepository.save(url);
	    return url;
	} else {
	    // Not able to create URL
	    throw new UrlCreationFailedException(urlString);
	}
    }

    public Url save(String urlString, String customUrl) throws UrlInvalidException {
	// 1. Validity Check
	boolean isValidUrl = Utility.isValidUrl(urlString);
	boolean isValidCustomUrl = Utility.isValidCustomUrl(customUrl);
	if (!isValidUrl) {
	    throw new UrlInvalidException(urlString);
	}
	if (!isValidCustomUrl) {
	    throw new UrlInvalidException(customUrl);
	}
	// 3. Create url
	Url url = null;
	// 3.a Check if short url already present
	List<Url> urls = urlRepository.findByShortUrl(customUrl);
	if (urls.isEmpty() && !customUrl.isEmpty()) {
	    url = new Url(urlString, customUrl,true);
	} else {
	    throw new UrlDuplicateException(customUrl);
	}
	{
	    urlRepository.save(url);
	    return url;
	}
    }

    public Url findByShortUrl(String shortUrl) {
	ArrayList<Url> urls = (ArrayList<Url>) urlRepository.findByShortUrl(shortUrl);
	if (urls.isEmpty()) {
	    return null;
	}
	return urls.get(urls.size() - 1);
    }

}
