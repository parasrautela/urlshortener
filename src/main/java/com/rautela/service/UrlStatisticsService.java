package com.rautela.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rautela.bean.Url;
import com.rautela.bean.UrlStatistics;
import com.rautela.exception.UrlInvalidException;
import com.rautela.exception.UrlNotFoundException;
import com.rautela.repository.UrlRepository;
import com.rautela.repository.UrlStatisticsRepository;

@Service
public class UrlStatisticsService {

    Logger logger = LoggerFactory.getLogger(UrlStatisticsService.class);

    @Autowired
    private UrlStatisticsRepository urlStatisticsRepository;
    @Autowired
    private UrlRepository urlRepository;
    public List<UrlStatistics> findAll() {
	ArrayList<UrlStatistics> urlStatistics = (ArrayList<UrlStatistics>) urlStatisticsRepository.findAll();
	return urlStatistics;
    }

    public UrlStatistics findById(Long id) throws UrlNotFoundException {
	UrlStatistics urlStatistics = urlStatisticsRepository.findById(id)
		.orElseThrow(() -> new UrlNotFoundException(id));
	return urlStatistics;
    }

    public List<UrlStatistics> findByShortUrl(String shortUrl) throws UrlNotFoundException {
	List<Url> urls = urlRepository.findByShortUrl(shortUrl);
	if(!urls.isEmpty()) {
	    List<UrlStatistics> urlStatistics = urlStatisticsRepository.findByUrl(urls.get(0));
	    return urlStatistics;	
	}else {
	    throw new UrlNotFoundException(shortUrl);
	}
	
    }

    public void deleteById(Long id) {
	urlStatisticsRepository.deleteById(id);

    }

    public UrlStatistics save(Url url,String sourceIp,String sourceDevice) throws UrlInvalidException {
	UrlStatistics entity = new UrlStatistics(url, sourceIp, sourceDevice,new Date());
	urlStatisticsRepository.save(entity);
	return entity;
    }

}
