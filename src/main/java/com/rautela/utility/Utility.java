package com.rautela.utility;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.validator.routines.UrlValidator;
import org.jboss.logging.Logger;

public class Utility {

    static Logger logger = Logger.getLogger(Utility.class);

    public static boolean isValidUrl(String url) {
	String[] schemes = { "http", "https" };
	UrlValidator urlValidator = new UrlValidator(schemes);
	boolean result;
	if (urlValidator.isValid(url)) {
	    result = true;
	} else {
	    result = false;
	}
	logger.info("Url >>" + url + "<< \tisValid:" + result);
	return result;
    }

    public static String getShortUrl(String urlString, int retryCount) {
	MessageDigest messageDigest;
	try {
	    messageDigest = MessageDigest.getInstance("MD5");

	    messageDigest.update(urlString.getBytes(StandardCharsets.UTF_8));
	    byte[] hashCode = messageDigest.digest();
	    StringBuilder hash = new StringBuilder();
	    for (byte b : hashCode) {
		hash.append(String.format("%02x", b));
	    }

	    return hash.substring(retryCount, retryCount + 6);
	} catch (NoSuchAlgorithmException e) {
	    e.printStackTrace();
	}
	return "";
    }

    public static void main(String[] args) {
	String url = "www.google.com";
	System.out.println(getShortUrl(url, 0));
    }

    public static boolean isValidCustomUrl(String customUrl) {
	boolean result;
	if (customUrl != null && !customUrl.isEmpty() && customUrl.length() > 2 && customUrl.length() < 15)
	    result = true;
	else
	result = false;
	logger.info("CustomUrl >>" + customUrl + "<< \tisValidCustomUrl:" + result);

	return result;
    }

}
